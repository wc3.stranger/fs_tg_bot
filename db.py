import json
import functools
import sqlite3


DATABASE = None


class Database:
    conn = None

    def __init__(self, db_file):
        self.connect(db_file)

    def connect(self, db_file):
        self.conn = sqlite3.connect(db_file, check_same_thread=False)
        # @note: json1 extension is already built with python
        self.ensure_tables()

        return self.conn

    def ensure_tables(self):
        c = self.conn.cursor()

        c.executescript("""
            CREATE TABLE IF NOT EXISTS user_settings (
                command VARCHAR(20),
                user_id INTEGER NOT NULL,
                settings JSON NOT NULL,
                UNIQUE(command, user_id) ON CONFLICT ROLLBACK
            );
            CREATE TABLE IF NOT EXISTS active_command (
                command VARCHAR(20) NOT NULL,
                user_id INTEGER NOT NULL,
                UNIQUE(user_id) ON CONFLICT ROLLBACK
            );
            CREATE TABLE IF NOT EXISTS serve_files (
                user_id INTEGER NOT NULL,
                chat_id INTEGER NOT NULL,
                msg_id INTEGER NOT NULL,
                f_path VARCHAR(2048) NOT NULL,
                f_info JSON,
                UNIQUE(chat_id, msg_id) ON CONFLICT ROLLBACK
            );
        """)

        self.conn.commit()
        c.close()
        del(c)

    @functools.lru_cache(maxsize=64)
    def get_glob_settings(self, user_id):
        c = self.conn.cursor()
        if user_id:
            c.execute(
                "SELECT settings FROM user_settings WHERE command IS NULL AND user_id = ?",
                (user_id,),
            )
        else:
            c.execute("""
                SELECT settings FROM user_settings
                WHERE command IS NULL AND json_extract(settings, '$.is_root') = ?""",
                (True,)
            )

        row = c.fetchone()
        c.close()
        del(c)

        return json.loads(row[0]) if row else None

    @functools.lru_cache(maxsize=64)
    def get_settings(self, command, user_id):
        if command is None:
            return self.get_glob_settings(user_id)

        c = self.conn.cursor()
        c.execute(
            "SELECT settings FROM user_settings WHERE command = ? AND user_id = ?",
            (command, user_id),
        )
        row = c.fetchone()
        c.close()
        del(c)

        return json.loads(row[0]) if row else None

    def create_settings(self, command, user_id, settings):
        assert user_id is not None

        c = self.conn.cursor()
        c.execute(
            "INSERT INTO user_settings (command, user_id, settings) VALUES (?, ?, json(?))",
            (command, user_id, json.dumps(settings),),
        )
        self.conn.commit()
        c.close()
        del(c)

        # @note: invalidate caches
        self.get_glob_settings.cache_clear()
        self.get_settings.cache_clear()

    def update_settings(self, command, user_id, settings):
        assert user_id is not None

        current_settings = self.get_settings(command, user_id)
        if not current_settings:
            raise RuntimeError('Settings for command {} and user id {} not exists'.format(
                command, user_id,
            ))

        current_settings.update(settings)
        c = self.conn.cursor()
        if command is None:
            # @note: global settings
            c.execute(
                "UPDATE user_settings SET settings = json(?) WHERE command IS NULL AND user_id = ?",
                (json.dumps(current_settings), user_id,),
            )
        else:
            # @note: command settings
            c.execute(
                "UPDATE user_settings SET settings = json(?) WHERE command = ? AND user_id = ?",
                (json.dumps(current_settings), command, user_id,),
            )

        self.conn.commit()
        c.close()
        del(c)

        # @note: invalidate caches
        self.get_glob_settings.cache_clear()
        self.get_settings.cache_clear()

        return current_settings

    def delete_settings(self, user_id, command=None):
        c = self.conn.cursor()
        if command:
            c.execute(
                "DELETE FROM user_settings WHERE user_id = ? AND command = ?",
                (user_id, command,),
            )
        else:
            c.execute("DELETE FROM user_settings WHERE user_id = ?", (user_id,))

        self.conn.commit()
        c.close()
        del(c)

        # @note: invalidate caches
        self.get_glob_settings.cache_clear()
        self.get_settings.cache_clear()

    @functools.lru_cache(maxsize=32)
    def get_active_command(self, user_id):
        c = self.conn.cursor()
        c.execute("SELECT command FROM active_command WHERE user_id = ?", (user_id,))
        row = c.fetchone()
        c.close()
        del(c)

        return row[0] if row else None

    def update_active_command(self, user_id, command=None):
        c = self.conn.cursor()
        if not command:
            c.execute("DELETE FROM active_command WHERE user_id = ?", (user_id,))
        else:
            last_command = self.get_active_command(user_id)
            if last_command:
                c.execute(
                    "UPDATE active_command SET command = ? WHERE user_id = ?",
                    (command, user_id,)
                )
            else:
                c.execute(
                    "INSERT INTO active_command (command, user_id) VALUES (?, ?)",
                    (command, user_id,)
                )

        self.conn.commit()
        c.close()
        del(c)

        # @note: invalidate cache
        self.get_active_command.cache_clear()

    def get_served_file(self, f_path):
        c = self.conn.cursor()
        c.execute(
            "SELECT chat_id, msg_id FROM serve_files WHERE f_path = ? LIMIT 1",
            (f_path,)
        )
        row = c.fetchone()
        c.close()
        del(c)
        return row[0] if row else None

    def get_served_file_by_ids(self, chat_id, msg_id):
        c = self.conn.cursor()
        c.execute(
            "SELECT f_path, f_info FROM serve_files WHERE chat_id = ? AND msg_id = ? LIMIT 1",
            (chat_id, msg_id)
        )
        row = c.fetchone()
        c.close()
        del c
        return (row[0], json.loads(row[1])) if row else None

    def save_served_file(self, user_id, chat_id, msg_id, f_path, f_info):
        c = self.conn.cursor()
        c.execute(
            """INSERT INTO serve_files (user_id, chat_id, msg_id, f_path, f_info)
               VALUES (?, ?, ?, ?, json(?))
            """,
            (user_id, chat_id, msg_id, f_path, json.dumps(f_info)),
        )
        self.conn.commit()
        c.close()
        del(c)

    def delete_served_file(self, chat_id, msg_id):
        c = self.conn.cursor()
        c.execute(
            "DELETE FROM serve_files WHERE chat_id = ? AND msg_id = ?",
            (chat_id, msg_id,)
        )
        self.conn.commit()
        c.close()
        del(c)

    def delete_served_files_all(self):
        c = self.conn.cursor()
        c.execute("DELETE FROM serve_files")
        self.conn.commit()
        c.close()
        del(c)

    def close(self):
        try:
            self.conn.close()
        except Exception as e:
            print('exception raised while closing db connection', e)


def init_db(db_file):
    global DATABASE
    DATABASE = Database(db_file)
