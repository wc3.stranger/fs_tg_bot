#include <Python.h>
#include <syscall.h>
#include <unistd.h>


static PyObject *
gettid_get(PyObject *self, PyObject *args)
{
    pid_t tid = (pid_t)syscall(SYS_gettid);
    return PyLong_FromLong(tid);
}

static PyMethodDef cGetTidMethods[] = {
    {"get",  gettid_get, METH_VARARGS,
     "Return current thread ID."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef cgettidmodule = {
   PyModuleDef_HEAD_INIT,
   "gettid",	    /* name of module */
   NULL,			/* module documentation, may be NULL */
   -1,				/* size of per-interpreter state of the module,
                       or -1 if the module keeps state in global variables. */
   cGetTidMethods
};

PyMODINIT_FUNC
PyInit_gettid(void)
{
    return PyModule_Create(&cgettidmodule);
}
