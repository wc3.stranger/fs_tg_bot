from distutils.core import setup, Extension


module1 = Extension(
    'gettid',
    sources=['gettid.c'],
)

setup(
    name='gettid',
    version='1.0a',
    author='KVN',
    author_email='warcraft.stranger@gmail.com',
    description='Extra extension written on C/C++',
    long_description='Extra extension written on C/C++',
    platforms=['Linux'],
    license='GPL-3',
    ext_modules=[module1],
)
