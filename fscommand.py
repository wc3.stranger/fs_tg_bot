import os
import re
import json
import html
import hashlib
import datetime
import traceback
import subprocess
import urllib.parse
import mimetypes
import tempfile
import asyncio

from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup, InputMediaPhoto
from telegram.ext import (
    CommandHandler, MessageHandler, CallbackQueryHandler, filters, CallbackContext,
)
from telegram.constants import ParseMode, MessageEntityType
from telegram.helpers import escape_markdown

import db
import config
from utils import *


mimetypes.init()

PREVIEWS_IMAGES_COUNT = 5
# @note: use None if do not need scaling for width and/or height
PREVIEW_MAX_WIDTH = 640
PREVIEW_MAX_HEIGHT = 480


def length_to_str(l):
    if l < 60:
        return f'00:{int(l):0>2}'
    elif l < 3600:
        return f'{int(l/60):0>2}:{int(l%60):0>2}'
    else:
        return f'{int(l/3600):0>2}:{int(l%3600/60):0>2}:{int(l%60):0>2}'


def human_size_str(s):
    if s < 100:
        return f"{s}b"
    elif s < 1024**2:
        return f"{s/1024:0>.2f}Kb"
    elif s < 1024**3:
        return f"{s/1024**2:0>.2f}Mb"
    else:
        return f"{s/1024**3:0>.2f}Gb"


def get_vfile_info(f_path) -> dict:
    res = subprocess.run(["ffprobe", "-v", "quiet", "-show_entries",
                          "stream=width,height : format=duration", "-of",
                          "default=nw=1:nk=1", f_path],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    try:
        (w, h, s) = res.stdout.strip().splitlines()
        return {
            'width': int(w), 'height': int(h), 'length': length_to_str(float(s)),
            'length_seconds': float(s)
        }
    except:
        traceback.print_exc()
        return {}


def get_afile_info(f_path) -> dict:
    res = subprocess.run(["ffprobe", "-v", "quiet", "-show_entries",
                          "format=duration", "-of",
                          "default=nw=1:nk=1", f_path],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    try:
        s = float(res.stdout.strip())
        return {'length': length_to_str(s), 'length_seconds': s}
    except:
        traceback.print_exc()
        return {}


def get_file_info(f_path):
    (m_type, m_enc) = mimetypes.guess_type(f_path)
    dict_info = {'mime_type': m_type, 'mime_encoding': m_enc}
    f_info_str = None

    if m_type and m_type.startswith('video/'):
        info = get_vfile_info(f_path)
        if info:
            dict_info.update(info)
            f_info_str = f'{info["length"]} / {info["width"]}x{info["height"]}'
    elif m_type and m_type.startswith('audio/'):
        info = get_afile_info(f_path)
        if info:
            dict_info.update(info)
            f_info_str = f'{info["length"]}'

    return dict_info, f_info_str or ''


class FileSystemCommand:
    COMMAND = 'fs'
    SALT = 'du+WW%cQF615]WZf>J3yiO]FZr_Gf^'

    def __init__(self):
        self.handlers = (
            CommandHandler(self.COMMAND, self.handle_command),
            CallbackQueryHandler(
                self.button_clicked,
                pattern='^{}/([0-9a-f]{{40}}|page_(\d+|prev|next)|hidden_(0|1)|dirs_(0|1)'
                        '|files_(0|1)|archive_(0|1))$'.format(self.COMMAND)
            ),
            CallbackQueryHandler(self.click_delete_message, pattern='^delete$'),
            CallbackQueryHandler(self.click_send_previews, pattern='^previews$'),
            MessageHandler(filters.Document.ALL, self.handle_file),
            MessageHandler(filters.TEXT, self.handle_text),
        )

    def ensure_user_settings(self, user_id):
        settings = self._get_settings(user_id)
        if not settings:
            settings = {
                'current_path': os.path.expanduser('~'),
                'columns': 3,
                'rows': 4,
                'page_buttons': 6,
                'hidden': False,
                'files': False,
                'dirs': True,
                'archive': False,
            }
            db.DATABASE.create_settings(self.COMMAND, user_id, settings)

        return settings

    def _get_settings(self, user_id):
        return db.DATABASE.get_settings(self.COMMAND, user_id)

    def _update_settings(self, user_id, settings):
        return db.DATABASE.update_settings(self.COMMAND, user_id, settings)

    def _current_path(self, user_id):
        return self._get_settings(user_id)['current_path']

    def get_dirs(self, base_dir, hidden, with_hash=True, prefix=None, **kwargs):
        objects = ['..']
        files = []
        try:
            for d in sorted(os.listdir(base_dir)):
                f_path = os.path.join(base_dir, d)
                if hidden or (not hidden and not d.startswith('.')):
                    if kwargs.get('dirs') and os.path.isdir(f_path):
                        objects.append(d)
                    elif kwargs.get('files') and not os.path.isdir(f_path):
                        files.append(d)
        except:
            traceback.print_exc()

        objects.extend(files)
        del(files)
        if not with_hash:
            return objects

        ret = []
        for obj in objects:
            ret.append((
                '{}{}'.format(
                    prefix or '',
                    hashlib.sha1('{}{}'.format(self.SALT, obj).encode()).hexdigest(),
                ), obj,
            ))

        return ret

    @print_exceptions
    @check_user(COMMAND)
    async def handle_command(self, update: Update, context: CallbackContext):
        print('handle {} command'.format(self.COMMAND))
        if not update.message:
            return

        user_id = update.effective_user.id

        # @note: set default settings for user if they're not set
        self.ensure_user_settings(user_id)

        (command, *c_args) = update.message.text.strip().split(' ', maxsplit=1)
        if not c_args:
            db.DATABASE.update_active_command(user_id, command=self.COMMAND)
            await self._send_dir_list(update.message, user_id, page=0, edit=False)
        else:
            (sub_command, *sc_args) = c_args[0].split(' ', maxsplit=1)
            await self.exec_subcommand(update, user_id, sub_command, *sc_args)

    async def exec_subcommand(self, update: Update, user_id, command, *args):
        # @note: command settings (rows, columns & page buttons)
        is_ok = False

        if command == 'set' and args:
            m_cols = re.search('\s+c(\d+)', ' {}'.format(args[0]))
            if m_cols and m_cols.group(1) != '0':
                self._update_settings(user_id, {'columns': int(m_cols.group(1))})
                is_ok = True

            m_rows = re.search('\s+r(\d+)', ' {}'.format(args[0]))
            if m_rows and m_rows.group(1) != '0':
                self._update_settings(user_id, {'rows': int(m_rows.group(1))})
                is_ok = True

            m_buttons = re.search('\s+b(\d+)', ' {}'.format(args[0]))
            if m_buttons and m_buttons.group(1) not in ('0', '1'):
                self._update_settings(user_id, {'page_buttons': int(m_buttons.group(1))})
                is_ok = True

        text = ''
        if not is_ok and args:
            text = 'Invalid params\n'
        text = '{}Settings:\n{}'.format(
            text, json.dumps(self._get_settings(user_id)),
        )

        await update.message.reply_text(text)

    async def _send_dir_list(self, message, user_id, page=0, edit=False):
        settings = self.ensure_user_settings(user_id)

        # @note: page buttons (footer)
        list_objects = self.get_dirs(
            settings['current_path'], settings['hidden'],
            prefix='{}/'.format(self.COMMAND),
            dirs=settings['dirs'], files=settings['files'],
        )
        button_list = []
        for obj in list_objects:
            button_list.append(InlineKeyboardButton(obj[1], callback_data=obj[0]))

        # @note: pagination
        cnt_on_page = settings['columns'] * settings['rows']
        pages_cnt = (len(button_list) - 1) // cnt_on_page + 1
        # @note: if rows or columns were changed by user later
        if page + 1 >= pages_cnt:
            page = pages_cnt - 1

        page_buttons = []
        if pages_cnt > 1:
            page_buttons = [
                InlineKeyboardButton(
                    '{}{}'.format('● ' if i == page else '', i + 1),
                    callback_data='{}/page_{}'.format(self.COMMAND, i),
                ) for i in range(pages_cnt)
            ]

        # @todo: build (limit) page buttons separately
        pass

        # @note: header buttons
        header_buttons = []
        if settings['hidden']:
            header_buttons.append(InlineKeyboardButton(
                '- Hidden', callback_data='{}/hidden_0'.format(self.COMMAND),
            ))
        else:
            header_buttons.append(InlineKeyboardButton(
                '+ Hidden', callback_data='{}/hidden_1'.format(self.COMMAND),
            ))

        if settings['files']:
            header_buttons.append(InlineKeyboardButton(
                '- Files', callback_data='{}/files_0'.format(self.COMMAND),
            ))
        else:
            header_buttons.append(InlineKeyboardButton(
                '+ Files', callback_data='{}/files_1'.format(self.COMMAND),
            ))

        if settings['dirs']:
            header_buttons.append(InlineKeyboardButton(
                '- Dirs', callback_data='{}/dirs_0'.format(self.COMMAND),
            ))
        else:
            header_buttons.append(InlineKeyboardButton(
                '+ Dirs', callback_data='{}/dirs_1'.format(self.COMMAND),
            ))

        if settings['archive']:
            header_buttons.append(InlineKeyboardButton(
                '- Archive', callback_data='{}/archive_0'.format(self.COMMAND),
            ))
        else:
            header_buttons.append(InlineKeyboardButton(
                '+ Archive', callback_data='{}/archive_1'.format(self.COMMAND),
            ))

        reply_markup = InlineKeyboardMarkup(build_menu(
            button_list[page * cnt_on_page : (page + 1) * cnt_on_page],
            n_cols=settings['columns'], vertical=True,
            header_buttons=header_buttons, footer_buttons=page_buttons,
        ))

        # @note: title required otherwise you did not see anything
        if edit:
            try:
                await message.edit_text(
                    'Current path: {}'.format(settings['current_path']),
                    reply_markup=reply_markup,
                )
            except:
                # @note: avoid telegram.error.BadRequest "Message is not modified"
                traceback.print_exc()
        else:
            await message.reply_text(
                'Current path: {}'.format(settings['current_path']),
                reply_markup=reply_markup,
            )

    @print_exceptions
    @check_user(COMMAND)
    async def click_delete_message(self, update: Update, context: CallbackContext):
        print('handle delete click for {} command'.format(self.COMMAND))
        await update.callback_query.answer()

        await context.bot.delete_message(
            update.effective_message.chat_id, update.callback_query.message.message_id,
        )
        db.DATABASE.delete_served_file(
            update.effective_message.chat_id,
            update.callback_query.message.message_id
        )

    @print_exceptions
    @check_user(COMMAND)
    async def click_send_previews(self, update: Update, context: CallbackContext):
        print('handle preview click for {} command'.format(self.COMMAND))
        await update.callback_query.answer()

        row = db.DATABASE.get_served_file_by_ids(
            update.effective_message.chat_id, update.callback_query.message.message_id
        )
        if not row:
            print('no served file in database, delete message?..')
            return

        warn_msg = await context.bot.send_message(
            update.effective_message.chat_id,
            "<i>Please wait while generating previews...</i>",
            parse_mode=ParseMode.HTML
        )

        step_seconds = row[1]['length_seconds'] / (PREVIEWS_IMAGES_COUNT + 1)
        start_ts = step_seconds / 2

        # @note: check if scale need
        w, h = row[1]['width'], row[1]['height']
        if PREVIEW_MAX_WIDTH and w > PREVIEW_MAX_WIDTH:
            scale_factor = w / PREVIEW_MAX_WIDTH
            w = PREVIEW_MAX_WIDTH
            h = int(h / scale_factor)
        if PREVIEW_MAX_HEIGHT and h > PREVIEW_MAX_HEIGHT:
            scale_factor = h / PREVIEW_MAX_HEIGHT
            h = PREVIEW_MAX_HEIGHT
            w = int(w / scale_factor)

        # print(f'scale from {row[1]["width"]}x{row[1]["height"]} to {w}x{h}')
        if row[1]['width'] != w or row[1]['height'] != h:
            ffmpeg_vf_opt = f"fps=1/{int(step_seconds+1)},scale={w}x{h}"
        else:
            ffmpeg_vf_opt = f"fps=1/{int(step_seconds+1)}"

        with tempfile.TemporaryDirectory() as tmpdir:
            proc = await asyncio.create_subprocess_exec(
                "ffmpeg", "-v", "quiet", "-ss", str(start_ts), "-i", row[0],
                "-filter:v", ffmpeg_vf_opt, f"{os.path.join(tmpdir, 'img%02d.jpg')}",
                stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.STDOUT,
                stdin=None,
            )
            proc_output, _ = await proc.communicate()
            await proc.wait()
            if proc.returncode != 0:
                print('error code while creating preview(s):', proc.returncode)
                await warn_msg.edit_text(
                    f'Error code {res.returncode} from ffmpeg while creating preview(s)',
                    parse_mode=ParseMode.HTML
                )
            else:
                await warn_msg.delete()
                # @note: now send'em all
                await context.bot.send_media_group(
                    chat_id=update.effective_message.chat_id,
                    media=[
                        InputMediaPhoto(media=open(os.path.join(tmpdir, f_name), 'rb')) \
                        for f_name in sorted(os.listdir(tmpdir))
                    ],
                    reply_to_message_id=update.callback_query.message.message_id
                )

    @print_exceptions
    @check_user(COMMAND)
    async def button_clicked(self, update: Update, context: CallbackContext):
        print('handle button click for {} command'.format(self.COMMAND))

        user_id = update.effective_user.id
        cb_data = update.callback_query.data
        dir_hash = cb_data.split('/')[1].strip()

        settings = self.ensure_user_settings(user_id)

        kwargs = {'edit': True, 'page': 0}
        if re.match('^page_\d+$', dir_hash):
            # @note: view selected page
            page_num = int(dir_hash.split('_')[1])
            kwargs.update({'page': page_num})
        elif re.match('^hidden_(0|1)$', dir_hash):
            settings = self._update_settings(
                user_id, {'hidden': dir_hash.split('_')[1] == '1'},
            )
        elif re.match('^dirs_(0|1)$', dir_hash):
            settings = self._update_settings(
                user_id, {'dirs': dir_hash.split('_')[1] == '1'},
            )
        elif re.match('^files_(0|1)$', dir_hash):
            settings = self._update_settings(
                user_id, {'files': dir_hash.split('_')[1] == '1'},
            )
        elif re.match('^archive_(0|1)$', dir_hash):
            settings = self._update_settings(
                user_id, {'archive': dir_hash.split('_')[1] == '1'},
            )
        else:
            # @note: cd to selected directory or send selected file
            list_dirs = dict(self.get_dirs(
                settings['current_path'], settings['hidden'], dirs=settings['dirs'],
                files=settings['files'],
            ))
            if dir_hash in list_dirs:
                obj_path = os.path.normpath(os.path.join(
                    settings['current_path'], list_dirs[dir_hash],
                ))
                if settings['archive']:
                    # @todo: download selected file/dir as archive
                    await update.callback_query.answer(text='<pre><Loading archive is not implepented></pre>')
                    return
                elif os.path.isdir(obj_path):
                    # @note: cd to selected directory
                    settings = self._update_settings(user_id, {'current_path': obj_path})
                else:
                    # @note: send selected file or link
                    await update.callback_query.answer()
                    f_size = os.path.getsize(obj_path)
                    if f_size / 1024**2 >= 50:
                        f_info, f_info_str = get_file_info(obj_path)
                        f_info['size'] = f_size
                        if f_info_str:
                            f_info_str = '{} / {}'.format(human_size_str(f_size), f_info_str)
                        else:
                            f_info_str = human_size_str(f_size)
                        msg = '{4}\nLocal [{0}]({2}/{1})\nGlobal [{0}]({3}/{1})'.format(
                            escape_markdown(os.path.basename(obj_path), version=2),
                            escape_markdown(
                                urllib.parse.quote_plus(obj_path), version=2,
                                entity_type='text_link'
                            ),
                            config.main_app['local_net'],
                            config.main_app['global_net'],
                            escape_markdown(f_info_str, version=2)
                        )

                        btns = []
                        if f_info['mime_type'] and f_info['mime_type'].startswith('video/'):
                            btns.append(InlineKeyboardButton('Previews', callback_data='previews'))
                        btns.append(InlineKeyboardButton('Delete', callback_data='delete'))
                        msg = await context.bot.send_message(
                            update.effective_message.chat_id,
                            msg,
                            parse_mode=ParseMode.MARKDOWN_V2,
                            disable_web_page_preview=True,
                            reply_markup=InlineKeyboardMarkup([btns]),
                        )
                        db.DATABASE.save_served_file(
                            user_id, update.effective_message.chat_id, msg.message_id,
                            obj_path, f_info
                        )
                    else:
                        msg = await context.bot.send_document(
                            update.effective_chat.id, open(obj_path, 'rb'), read_timeout=60.0,
                        )
                    return
            else:
                await update.callback_query.answer(text='<pre><Selected path does not exist></pre>')
                return

        await self._send_dir_list(update.callback_query.message, user_id, **kwargs)
        await update.callback_query.answer()

    @print_exceptions
    @check_active_command(COMMAND)
    @check_user(COMMAND)
    async def handle_file(self, update: Update, context: CallbackContext):
        for ent in update.message.caption_entities:
            if ent.type == MessageEntityType.BOT_COMMAND:
                return None

        user_id = update.effective_user.id
        settings = self.ensure_user_settings(user_id)

        doc = update.message.document
        f = await context.bot.get_file(doc.file_id)

        save_to = os.path.join(settings['current_path'], doc.file_name)
        print('download/save file id {} to {}'.format(doc.file_id, save_to))

        # @todo: check if the file exist and ask to overwrite or not
        pass

        # @warn: 3 minutes for the file download
        await f.download_to_drive(custom_path=save_to, read_timeout=180.0)
        await update.message.reply_text('<File saved to {}>'.format(save_to))

    @print_exceptions
    @check_active_command(COMMAND)
    @check_user(COMMAND)
    async def handle_text(self, update: Update, context: CallbackContext):
        print('handle text for {} command'.format(self.COMMAND))

        user_id = update.effective_user.id
        self.ensure_user_settings(user_id)

        process = subprocess.run(
            update.message.text, shell=True, check=False, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, cwd=self._current_path(user_id),
        )
        out = ''
        if process.stderr:
            out = '{}{}'.format(out, process.stderr.decode())
        if process.stdout:
            out = '{}{}'.format(out, process.stdout.decode())

        if not out:
            out = '<output is absent>'

        await update.message.reply_html('<pre>{}</pre>'.format(html.escape(out)[:4085]))
