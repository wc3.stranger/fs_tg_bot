import os
import sys
import configparser

from telegram.ext import Updater
from config import main_app as config


def main(is_webhook):
    # @note: config
    TOKEN = config['token']
    WEBHOOK_URL = config['webhook_url']

    updater = Updater(
        TOKEN, request_kwargs=config['request_kwargs'],
        use_context=True
    )
    info = updater.bot.get_webhook_info()
    print('current webhook info:', info)

    if is_webhook and (not info or not info.url or info.url != WEBHOOK_URL):
        #(listen_on_ip, listen_on_port) = config['listen_on'].split(':')
        #updater.start_webhook(listen=listen_on_ip, port=int(listen_on_port), url_path=TOKEN)
        updater.bot.set_webhook(
            url=WEBHOOK_URL,
            certificate=open(config['cert'], 'rb'),
            timeout=5.0,
            #allowed_updates=['message'],
        )
        print('new webhook is configured')
    elif is_webhook and info.pending_update_count > 0 and info.last_error_message is None:
        # @warn: clear off getUpdates
        print('reset webhook and setup again')
        updater.bot.set_webhook(url='')

        updates = updater.bot.get_updates()
        last_update_id = max([u.update_id for u in updates])
        updater.bot.get_updates(offset=last_update_id + 1)

        updater.bot.set_webhook(
            url=WEBHOOK_URL,
            certificate=open(config['cert'], 'rb'),
            timeout=5.0,
            #allowed_updates=['message'],
        )
        print('webhook is configured')
    elif not is_webhook and info and info.url:
        # @note: remove webhook
        updater.bot.set_webhook(url='')
        print('webhook removed')

    return 0


if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] not in ('webhook', 'polling',):
        print('Specify "polling" or "webhook" setup')
        sys.exit(0)

    sys.exit(main(sys.argv[1]=='webhook'))
