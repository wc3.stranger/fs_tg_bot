#!/usr/bin/env python3

import os
import datetime
import traceback
import json
import html

from telegram import Update
from telegram.ext import CommandHandler, CallbackContext, Application

import db
import config
from simplecommands import *
from hashcommand import HashCommand
from fscommand import FileSystemCommand


async def error_handler(update: Update, context: CallbackContext):
    """Log the error and send a telegram message to notify the developer."""
    print(
        "{} exception while handling an update:".format(datetime.datetime.now()),
        context.error
    )
    tb_list = traceback.format_exception(None, context.error, context.error.__traceback__)
    print("".join(tb_list))


def main():
    db.init_db(os.path.join(os.path.dirname(__file__), 'db.sqlite3'))
    app = Application.builder().token(config.main_app['token']).build()

    # @note: simple commands
    app.add_handler(CommandHandler('init', init_command))
    app.add_handler(CommandHandler('start', start_command))
    app.add_handler(CommandHandler('hello', hello_command))
    app.add_handler(CommandHandler('active', active_command))

    commands = (HashCommand(), FileSystemCommand(),)
    # @note: custom commands
    for ind, ch in enumerate(commands):
        for h in ch.handlers:
            # @note: grouping (need to iterate over handle_text for active command)
            app.add_handler(h, group=ind)

    app.add_error_handler(error_handler)

    # @note: pass webhook settings to telegram
    # @note: with nginx as reverse proxy
    (listen_on_ip, listen_on_port) = config.main_app['listen_on'].split(':')
    app.run_webhook(
        listen=listen_on_ip, port=int(listen_on_port), url_path=config.main_app['token'],
        drop_pending_updates=True, webhook_url=config.main_app['webhook_url'],
        cert=config.main_app['cert'],
    )


if __name__ == '__main__':
    main()
