from telegram import Update
from telegram.ext import CallbackContext

import db
from utils import print_exceptions


__all__ = ['start_command', 'hello_command', 'active_command', 'init_command']
__slots__ = tuple(__all__)


async def start_command(update: Update, context: CallbackContext):
    print('handle start command')
    if update.message:
        await update.message.reply_text('Welcome to file system bot, {}. Your user ID {}'.format(
            update.message.from_user.first_name, update.message.from_user.id,
        ))


async def hello_command(update: Update, context: CallbackContext):
    print('handle hello command')
    if update.message:
        await update.message.reply_text('Hello, {}. Your user ID {}'.format(
            update.message.from_user.first_name, update.message.from_user.id,
        ))


async def active_command(update: Update, context: CallbackContext):
    print('handle active command')
    if not update.effective_user:
        return

    c = db.DATABASE.get_active_command(update.effective_user.id) or '-'
    await update.message.reply_text('Active command: {}'.format(c))


@print_exceptions
async def init_command(update: Update, context: CallbackContext):
    print('handle init command')
    if not update.effective_user:
        return

    glob_settings = db.DATABASE.get_glob_settings(None)
    if glob_settings is None:
        user_id = update.effective_user.id
        db.DATABASE.create_settings(
            None, user_id, {'user_id': user_id, 'is_root': True, 'chat_id': None},
        )
        await update.message.reply_text(
            'Bot initialized for user ID {}'.format(user_id),
        )
    else:
        await update.message.reply_text('Error: bot is already initialized')
