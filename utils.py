import math
import traceback
import functools
import asyncio
import subprocess

from telegram import Update
from telegram.ext import CallbackContext
import db


__all__ = [
    'print_exceptions', 'build_menu', 'sizeof_fmt', 'check_active_command', 'check_user',
]
__slots__ = tuple(__all__)


def print_exceptions(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            traceback.print_exc()

    return wrapped


def build_menu(buttons, n_cols, vertical=False, header_buttons=None, footer_buttons=None):
    if vertical:
        n_rows = math.ceil(len(buttons) / n_cols)
        menu = [buttons[i::n_rows] for i in range(0, n_rows)]
    else:
        menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]

    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def sizeof_fmt(num, suffix='B'):
    for unit in ('', 'Ki', 'Mi', 'Gi', 'Ti',):
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def check_active_command(command):
    def decorated(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            user_id = None
            for a in args:
                if isinstance(a, Update) and a.effective_user:
                    user_id = a.effective_user.id

            if user_id:
                com = db.DATABASE.get_active_command(user_id)
                if com != command:
                    print('DEBUG: active command {} != {}'.format(com, command))
                    return
                else:
                    return func(*args, **kwargs)
            else:
                print('ERROR: user_id is not present')
        return wrapped
    return decorated


def check_user(command):
    def decorated(method):
        @functools.wraps(method)
        async def wrapped(self, update: Update, context: CallbackContext):
            user_id = update.effective_user.id if update.effective_user else None
            chat_id = update.effective_chat.id if update.effective_chat else None
            if not user_id:
                # @warn: bots not allowed anyway
                print('ERROR: user_id not present')
            else:
                settings = db.DATABASE.get_glob_settings(user_id)
                if settings is not None \
                        and (settings['is_root'] or command in settings['allowed_commands']):
                    if settings.get('chat_id') != chat_id:
                        db.DATABASE.update_settings(None, user_id, {'chat_id': chat_id})
                    return await method(self, update, context)
                else:
                    print(
                        'ERROR: permission denied for user', user_id,
                        'to execute command {}'.format(command),
                    )
                    # @note: we need to answer something to callback query
                    if update.callback_query is not None:
                        await update.callback_query.answer(text='<pre><Access denied></pre>')
        return wrapped
    return decorated

def pipe_commands(comm1, comm2):
    c1 = subprocess.Popen(comm1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    c2 = subprocess.run(
        comm2, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=c1.stdout
    )
    return c2


async def async_pipe_commands(comm1, comm2):
    proc1 = await asyncio.create_subprocess_exec(
        *comm1, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.STDOUT,
        stdin=None,
    )
    proc1_output, _ = await proc1.communicate()
    await proc1.wait()
    if proc1.returncode != 0:
        return proc1.returncode, None

    proc2 = await asyncio.create_subprocess_exec(
        *comm2, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.STDOUT,
        stdin=asyncio.subprocess.PIPE
    )
    proc2_stdout, _ = await proc2.communicate(proc1_output)
    await proc2.wait()
    return proc2.returncode, proc2_stdout
