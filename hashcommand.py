import hashlib

from telegram import Update
from telegram.ext import CommandHandler, MessageHandler, filters, CallbackContext

import db
from utils import print_exceptions, check_active_command, check_user


class HashCommand:
    COMMAND = 'hash'
    hashes = ('md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512',)

    def __init__(self):
        self.handlers = (
            CommandHandler(self.COMMAND, self.handle_command, block=False),
            MessageHandler(filters.TEXT, self.handle_text, block=False),
        )

    @print_exceptions
    @check_user(COMMAND)
    async def handle_command(self, update: Update, context: CallbackContext):
        print('handle {} command'.format(self.COMMAND))
        if not update.message:
            return

        user_id = update.effective_user.id

        (command, *args) = update.message.text.split(' ', maxsplit=1)
        if not args:
            db.DATABASE.update_active_command(user_id, command=self.COMMAND)
            await update.message.reply_text(
                '<hash> command activated. Enter text in format: <hash_type> <text>.'
                ' Available hash types: {}'.format(', '.join(self.hashes))
            )
            return

        # @note: "inline" command, do not switch "hash" as active command
        (sub_command, *args) = args[0].split(' ', maxsplit=1)
        if sub_command not in self.hashes:
            await update.message.reply_text('Select from the list: {}'.format(', '.join(self.hashes)))
        else:
            await update.message.reply_text(
                self.calculate_hash(sub_command, args[0] if args else ''),
            )

    @print_exceptions
    @check_active_command(COMMAND)
    @check_user(COMMAND)
    async def handle_text(self, update: Update, context: CallbackContext):
        print('handle text for {} command'.format(self.COMMAND))
        (sub_command, *args) = update.message.text.split(' ', maxsplit=1)
        if sub_command not in self.hashes:
            await update.message.reply_text('Select from the list: {}'.format(', '.join(self.hashes)))
        else:
            await update.message.reply_text(
                self.calculate_hash(sub_command, args[0] if args else '')
            )

    def calculate_hash(self, hash_name, text):
        func = getattr(hashlib, hash_name)
        return func((text or '').encode('utf8')).hexdigest()
