# File system browser (personal telegram bot)

## Requirements
- Python 3.6+ / pip
- Configured telegram bot by yourself (see here https://core.telegram.org/bots#6-botfather)

## Install/start (Linux)
```
$ git clone git@gitlab.com:wc3.stranger/fs_tg_bot.git
$ cd fs_tg_bot
$ python -m venv .venv
$ source .venv/bin/activate
$ pip install python-telegram-bot==13.10
or (if you want to work behind Sock5 server, e.g. Tor)
$ pip install python-telegram-bot[socks]==13.10

<configure your personal config.py file>

Starting bot (telegram server connects to your bot):
$ python start-webhooks.py
or (your bot connects to telegram server and check messages every 5 seconds)
$ python start-updates.py

Start/stop daemon for serving large files:
$ python serve-files.py start/stop
```

After successful bot start you should "connect" to your bot via usual telegram client (mobile or desktop).
Then you should type "/init" command to "initialize" bot so it would be "connected" to you and not to any other telegram user.
