import os
import sys
import atexit
import signal
import logging
import time


__author__ = 'kvn'
__copyright__ = 'kvn'
__all__ = ['Daemon', 'daemon_stop']


log = logging.getLogger('daemon')


class Daemon:
    """Generic daemon. Inherit from him and rewrite C{run} method
    """

    def __init__(self, pid_file, log_file):
        self.pid = None
        self.pid_file = pid_file
        self.stdin = os.devnull
        self.stdout = self.stderr = log_file

    def daemonize(self):
        """
        Do the UNIX double-fork magic, see Stevens' I{"Advanced Programming in
        the UNIX Environment"} for details (B{ISBN 0201563177}) on
        U{http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16}
        """

        log.info('daemonize')
        try:
            pid = os.fork()
            if pid > 0:
                # @note: exit first parent
                sys.exit()
        except SystemExit:
            #log.error('fork #1 failed')
            sys.exit(1)

        # @note: decouple from parent environment
        os.setsid()
        os.umask(0)

        # @note: do second fork
        try:
            pid = os.fork()
            if pid > 0:
                # @note: exit from second parent
                sys.exit()
        except SystemExit:
            #log.error('fork #2 failed')
            sys.exit(1)

        # @note: redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = open(self.stdin, 'rb')
        so = se = open(self.stdout, 'ab+', buffering=0)
        # se = file(self.stderr, 'ab+', buffering=0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

        # @note: write pid to pid-file
        atexit.register(self.delete_pid)
        self.pid = str(os.getpid())
        open(self.pid_file, 'w+').write('{}\n'.format(self.pid))
        log.info('pid file is {}'.format(self.pid_file))

    def delete_pid(self):
        """Delete pid-file
        """
        os.remove(self.pid_file)

    def start_it(self):
        """Start the daemon
        """

        # @note: check for a pid-file to see if the daemon already run
        if os.path.isfile(self.pid_file):
            pid = open(self.pid_file, 'rb').read(10).rstrip()
            if not pid.isdigit() or not os.path.isdir('/proc/{}'.format(pid.decode())):
                os.remove(self.pid_file)
            else:
                msg = 'daemon already running'.format(self.pid_file)
                log.info(msg); print(msg)
                return

        # @note: start the daemon
        log.info('starting daemon ...')
        self.daemonize()
        self.run()

    def run(self):
        """
        You should override this method when you subclass Daemon.
        It will be called after the process has been daemonized by
        C{start_it()}.
        """

        pass


def daemon_stop(pid_file):
    """Stopping daemon
    """

    # @note: get the pid from the pid-file
    if not os.path.isfile(pid_file):
        msg = 'pid-file {} is absent'.format(pid_file)
        log.info(msg); sys.stdout.write(msg + '\n')
        return 2

    log.info('stopping daemon'); sys.stdout.write('stopping daemon\n')
    try:
        pf = open(pid_file, 'r')
        pid = int(pf.read().strip())
        pf.close()
    except IOError:
        pid = None

    if not pid:
        msg = 'daemon is already stopped'
        log.warning(msg); sys.stdout.write(msg + '\n')
        return

    # @note: stop or kill daemon
    try:
        # @note: send SIGTERM for 30*0.3 = 9 seconds, otherwise - just kill him
        cnt = 30
        check_each = 0.3
        total_seconds = cnt * check_each
        while cnt > 0:
            os.kill(pid, signal.SIGTERM)
            time.sleep(0.3)
            cnt -= 1

        # @note: some blocking socket used, kill him immediately if he did not understood
        msg = 'daemon not stopped after {} seconds, kill him'.format(total_seconds)
        log.warning(msg); sys.stdout.write(msg + '\n')
        os.kill(pid, signal.SIGKILL)
    except ProcessLookupError:
        pass

    if os.path.exists(pid_file):
        os.remove(pid_file)
