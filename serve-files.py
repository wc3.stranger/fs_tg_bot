import os
import io
import mimetypes
import time
import logging
import traceback
import sys
import threading
import signal
import urllib.parse
from socketserver import TCPServer
from http.server import BaseHTTPRequestHandler

import db
import config
import gettid
from genericdaemon import Daemon, daemon_stop


class RequestHandler(BaseHTTPRequestHandler):
    """HTTP request handler.
    """

    def version_string(self):
        """Return version as header in all server responses.
        """
        return 'serve downloads'

    def _write_error(self, code, msg):
        self.send_response(code)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(msg.encode())

    def do_GET(self):
        """Accept GET request.
        """

        try:
            fpath = urllib.parse.unquote_plus(self.path[1:])
        except:
            fpath = self.path

        # @todo: check file existence by verify self.path
        if not os.path.isfile(fpath):
            return self._write_error(404, 'not found')
        elif not db.DATABASE.get_served_file(fpath):
            return self._write_error(404, 'forbidden')

        (m_type, m_encoding) = mimetypes.guess_type(fpath)
        if not m_type:
            m_type = 'application/octet-stream'

        self.send_response(200)
        self.send_header('Content-Type', m_type)
        self.send_header(
            'Content-Disposition',
            'attachment;filename={}'.format(os.path.basename(fpath))
        )
        self.end_headers()
        with open(fpath, 'rb') as f:
            while self.wfile.write(f.read(4096)):
                pass


class ThreadingMixIn:
    """Mix-in class to handle each request in a new thread.
    """

    # @note: decides how threads will act upon termination of the main process
    daemon_threads = False

    def process_request_thread(self, request, client_address):
        """Same as in BaseServer but as a thread.
        In addition, exception handling is done here.
        """

        try:
            self.finish_request(request, client_address)
            self.shutdown_request(request)
        except:
            self.handle_error(request, client_address)
            self.shutdown_request(request)

    def process_request(self, request, client_address):
        """Start a new thread to process the request
        """

        # @note: write here something (e.g. sent args to thread, etc)
        t = threading.Thread(
            target=self.process_request_thread, args=(request, client_address),
        )
        t.daemon = self.daemon_threads
        t.start()


class ThreadingTCPServer(ThreadingMixIn, TCPServer):
    pass


class DaemonServer(ThreadingTCPServer, Daemon):
    _time_start = None

    def __init__(self, server_address, handler_class, log_file, pid_file):
        ThreadingTCPServer.__init__(
            self, server_address, handler_class, bind_and_activate=False,
        )
        Daemon.__init__(self, pid_file, log_file)
        self.allow_reuse_address = 1

    def run(self):
        """Run server.
        """

        log.info('run web server ...')
        try:
            # @note: init db
            db.init_db(os.path.join(os.path.dirname(__file__), 'db.sqlite3'))
            # @note: bind and activate server
            self.server_bind()
            self.server_activate()
            log.info('listen on {}:{}'.format(*self.socket.getsockname()))
            # @note: save local start time
            self._time_start = time.localtime()
            self.serve_forever()
            return 1
        except:
            log.error('exception in <run>: {}'.format(traceback.format_exc()))

    def shutdown_server(self):
        def shutdown_it(*args, **kargs):
            self.shutdown()
            self.server_close()

        # @note: shutdown from another thread to avoid deadlock in serve_forever
        t = threading.Thread(target=shutdown_it)
        t.start()

    def handle_sigterm(self, signum, frame):
        """Signals handler: do not interrupt the daemon.
        """

        if hasattr(self, '__shutdown_in_progress'):
            return
        setattr(self, '__shutdown_in_progress', 1)
        log.warning('signal {} catched, stopping server'.format(signum))
        log.info('shutdown in progress')

        db.DATABASE.close()
        self.shutdown_server()
        log.info('server stopped')


class ContextFilter(logging.Filter):
    def filter(self, record):
        result = super(ContextFilter, self).filter(record)
        if result:
            # @note: inject real thread id into log instead of pthread_self
            record.tid = gettid.get()
            return result


def setup_logger(log_file):
    """@note: configure log.
    """

    log = logging.getLogger('daemon')
    log.setLevel(logging.DEBUG)
    handler = logging.FileHandler(log_file)
    fmt = logging.Formatter(
        '%(asctime)s %(tid)d %(levelname)s %(message)s'
    )
    handler.setFormatter(fmt)
    log.addHandler(handler)
    log.addFilter(ContextFilter())

    return log


def get_config():
    # @note: config
    if not config.serve_files['log_file'].startswith('/'):
        config.serve_files['log_file'] = os.path.join(
            os.path.dirname(__file__), config.serve_files['log_file']
        )
    if not config.serve_files['pid_file'].startswith('/'):
        config.serve_files['pid_file'] = os.path.join(
            os.path.dirname(__file__), config.serve_files['pid_file']
        )

    return config.serve_files


if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] not in ['start', 'stop']:
        print('start|stop')
        sys.exit()

    conf = get_config()
    log = setup_logger(conf['log_file'])
    command = sys.argv[1]
    if command == 'start':
        (srv_addr, srv_port) = conf['listen_on'].split(':')
        server = DaemonServer(
            (srv_addr, int(srv_port)), RequestHandler, conf['log_file'], conf['pid_file']
        )
        server.stdout = server.stderr = conf['log_file']
        # @note: "stop daemon" signal handler
        signal.signal(signal.SIGTERM, server.handle_sigterm)
        server.start_it()
    else:
        if daemon_stop(conf['pid_file']) != 2:
            log.info('daemon is down')
