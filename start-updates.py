#!/usr/bin/env python3
"""
init - init bot for the user which typed this command first
start - welcome message with some info
hello - hello message with some info
fs - upload file to local file system
shell - open terminal session (Linux only)
hash - calculate hash for md5, sha1, sha224, sha256, sha384 or sha512
active - show active command for user
"""

import os
import sys
import datetime
import json

from telegram.ext import Updater, CommandHandler, CallbackContext
from telegram.update import Update

__path__ = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, __path__)

import db
import config
from simplecommands import *
from hashcommand import HashCommand
from fscommand import FileSystemCommand
from shellcommand import ShellCommand


def error_handler(update: Update, context: CallbackContext):
    print('{} telegram error, context: {}'.format(datetime.datetime.now(), context))


if __name__ == '__main__':
    # @note: sqlite db
    db.init_db(os.path.join(os.path.dirname(__file__), 'db.sqlite3'))

    updater = Updater(
        config.main_app['token'], request_kwargs=config.main_app['request_kwargs'],
        use_context=True
    )
    # @note: simple commands
    updater.dispatcher.add_handler(CommandHandler('init', init_command))
    updater.dispatcher.add_handler(CommandHandler('start', start_command))
    updater.dispatcher.add_handler(CommandHandler('hello', hello_command))
    updater.dispatcher.add_handler(CommandHandler('active', active_command))

    commands = (HashCommand(), FileSystemCommand(), ShellCommand(),)

    # @note: custom commands
    for ind, ch in enumerate(commands):
        for h in ch.handlers:
            # @note: grouping (need to iterate over handle_text for active command)
            updater.dispatcher.add_handler(h, group=ind)

    updater.dispatcher.add_error_handler(error_handler)

    updater.start_polling(poll_interval=5.0, drop_pending_updates=True)
    print('bot started')
    updater.idle()
