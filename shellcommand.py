import os
import pty
import select
import hashlib

from telegram.update import Update
from telegram.ext import CommandHandler, MessageHandler, filters, CallbackContext

import db
from utils import print_exceptions, check_active_command, check_user


class ShellCommand:
    COMMAND = 'shell'
    # @warn: max message size is 4KiB (4096 bytes)
    MAX_LINES_DEFAULT = 20

    def __init__(self):
        self.handlers = (
            CommandHandler(self.COMMAND, self.handle_command),
            MessageHandler(filters.TEXT, self.handle_text),
        )

    def _get_settings(self, user_id):
        return db.DATABASE.get_settings(self.COMMAND, user_id)

    def _update_settings(self, user_id, settings):
        return db.DATABASE.update_settings(self.COMMAND, user_id, settings)

    def ensure_user_settings(self, user_id):
        settings = self._get_settings(user_id)
        if not settings:
            settings = {
                'terms': {},
                'path': os.path.expanduser('~'),
            }
            db.DATABASE.create_settings(self.COMMAND, user_id, settings)

        return settings

    @print_exceptions
    @check_user(COMMAND)
    def handle_command(self, update: Update, context: CallbackContext):
        print('handle {} command'.format(self.COMMAND))
        if not update.message:
            return

        user_id = update.effective_user.id

        self.ensure_user_settings(user_id)
        terms = self._get_settings(user_id)['terms']
        if terms:
            # @note: get any opened terminal
            # @todo: terminal may be already killed and fd can be busy by somebody else.
            #        That's why we should take care of him once it was opened.
            (master_fd, (master_name, stdin_fd)), *extra = terms.items()
        else:
            (master_fd, stdin_fd) = pty.openpty()
            master_name = os.ttyname(master_fd)
            self._update_settings(user_id, {'terms': {master_fd: (master_name, stdin_fd)}})
            # @note: spawn command shell
            # os.spawn(os.environ['SHELL'])

        db.DATABASE.update_active_command(update.effective_user.id, command=self.COMMAND)
        update.message.reply_text('<pre><shell activated></pre>')

    @print_exceptions
    @check_active_command(COMMAND)
    @check_user(COMMAND)
    def handle_text(self, update: Update, context: CallbackContext):
        print('handle text for {} command'.format(self.COMMAND))

        user_id = update.effective_user.id
        self.ensure_user_settings(user_id)

        update.message.reply_text('<pre><not implemented yet></pre>')
